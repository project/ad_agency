<?php phptemplate_comment_wrapper(NULL, $node->type); ?>


<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>" id="node-<?php print $node->nid; ?>">
  <?php if ($page == 0): ?> <h2 class="title"> <a href="<?php print $node_url ?>"><?php print $title ?></a> </h2> <?php endif; ?>

  <?php if ($picture) print $picture ?>

	<?php if ($submitted or $has_terms): ?> <div class="meta<?php if ($has_terms) : ?> with-taxonomy<?php endif; ?>">

		<?php if ($submitted): ?> 
			<div class="post-date">
				<span class="post-year"><?php print (format_date($node->created, 'custom', 'Y')) ?></span>
				<span class="post-month"><?php print (format_date($node->created, 'custom', 'M')) ?></span>
				<span class="post-day"><?php print (format_date($node->created, 'custom', 'd')) ?></span>
			</div>

			<span class="author"><?php print theme('username', $node); ?></span>  
		<?php endif; ?>

		<?php if ($taxonomy) : ?><?php print $terms; ?><?php endif; ?>

	</div>

  <?php endif; ?>

  <div class="content">
    <?php print $content?>
  </div>

  <?php if ($links): ?> <div class="links"> <?php print $links ?> </div> <?php endif; ?>

</div>

