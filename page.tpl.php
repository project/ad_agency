<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<?php /* different ids allow for separate theming of the home page */ ?>
<body class="<?php print $body_classes; ?>">
	<div id="wrapper">
		<!-- top info starts here -->
		<div id="header">
			<?php if ($logo): ?>
				<a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" /> </a>
			<?php endif; ?>
			<div id="hright"  class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
				<?php if ($primary_links): ?> <div id="primary" class="clear-block"> <?php print theme('links', $primary_links); ?> </div> <?php endif; ?>
				<?php if ($secondary_links): ?> <div id="secondary" class="clear-block"> <?php print theme('links', $secondary_links); ?> </div> <?php endif; ?>
			</div>
			<div id="title">
				<?php if ($site_name): ?>
					<h1 id='home'> <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?> </a> </h1>
				<?php endif; ?>
				<!--<p>Lorem Ipsum Dolor Sit Amet</p>-->
			</div>
		</div>
		<!-- main content starts here -->
		<div id="homepic">
			<div class="message">
				<?php if ($site_name): ?>
					<h4 id='site-name'> <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?> </a> </h4>
				<?php endif; ?>
				<?php if ($site_slogan): ?>
					<p id='site-slogan'> <?php print $site_slogan; ?> </p>
				<?php endif; ?>
			</div>
		</div>
		<div id="bottomcontenttop"></div>
		<div id="bottomcontent">
		    <div id="container" class="clear-block">
				<!-- .......... sidebar-left .......... -->
				<div class="left">
					<?php if ($sidebar_left): ?>
						<div id="sidebar-left" class="column sidebar"> <?php print $sidebar_left; ?> </div>
					<?php endif; ?>
				</div>
				<!-- .......... sidebar-left .......... -->
				<!-- .......... main > squeeze .......... -->
				<div id="main" class="middle column">
					<div id="squeeze">
						<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
						<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
						<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
						<?php print $help; ?>
						<?php print $messages; ?>
						<?php print $content; ?>
					</div>
				</div>
				<!-- .......... /main .......... -->
				<!-- .......... sidebar-right .......... -->
				<?php if ($sidebar_right): ?>
				<div id="sidebar-right" class="column sidebar"> <?php print $sidebar_right; ?> </div>
				<?php endif; ?>
				<!-- .......... /sidebar-right .......... -->
			</div>
		</div>
		<div id="bottomcontentbtm"></div>
		<div id="footer">
				<p>Subscribe to this feed ... <?php print $feed_icons; ?></p>
				<p><?php print $footer_message; ?></p>
		</div>
	</div>
</body>
</html>
